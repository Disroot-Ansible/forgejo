# Forgejo - Ansible Role

This role covers deployment, configuration and software updates of Forgejo. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.
`vagrant up`

`ansible-playbook -b Playbooks/forgejo.yml`

Then you can then access forgejo from your computer on `http://192.168.33.14:3000`

The first user you create becomes the admin.

# Gitea or Forgejo
This role allows for deployment of gitea and forgejo. By default the role deploys forgejo and this is the flavor that is supported and maintained. Installing Gitea using the role is possible however starting from version 1.18.2 is no longer maintained by Disroot team.
To switch to gitea, update following variables in your Playbook/host_vars:


```
forgejo_flavour: 'gitea'
forgejo_version: '1.18.2'
forgejo_arch: 'linux-amd64'
forgejo_download_url: 'https://github.com/go-gitea/gitea/releases/download/v{{ forgejo_version }}/gitea-{{ forgejo_version }}-{{ forgejo_arch }}'
```

## Playbook
The playbook includes postgresql role and deploys entire stack needed to run Forgejo. Additional roles are also available in the Ansible roles repos in git.

## Customization
You can deploy custom themes with this role. To do so, uncomment `forgejo_theme_dir` and `forgejo_custom_themes`  from `defaults/main.yml`. Add the theme repos' address and name there, enable them and then deploy with `ansible-playbook -b Playbooks/forgejo.yml --tags theme`.

You can also add you own images like favicon, icons, etc. See https://forgejo.org/docs/v1.21/developer/customization/ to see how to prepare your images.
Then, copy them in the `img` folder of this role and uncomment `forgejo_extra_files_path` and `forgejo_theme_dir` from `defaults/main.yml`. You can then deploy with `ansible-playbook -b Playbooks/forgejo.yml --tags config`.

For more information about customizaton, check https://forgejo.org/docs/v1.21/developer/customization/
